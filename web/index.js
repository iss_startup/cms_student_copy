
var apiUrl = 'http://localhost:8000/api/';
var app = angular.module("MyCMSApp", ["ui.router"]);

app.config(["$stateProvider", "$urlRouterProvider", MyCMSConfig]);

app.controller("MyCMSCtrl", [MyCMSCtrl]);

//API Get service
app.factory('gettingFactory', ['$http', function($http){
    var factory = {};
    factory.getter = function(url){
        return $http.get(apiUrl + url);
    };
    factory.test = function(){
        console.log('connected to service')
    };

    return factory;
}]);

//API Delete Service
app.factory('deletingFactory', ['$http', '$window', function($http, $window){
    var factory = {};
    factory.deleter = function(mail, handler, redirectTo){
        $http({
            method: 'POST',
            url: apiUrl + handler + '/del',
            data: $.param(mail),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(){
                console.log('Post Successful');
                $window.location.reload();
            },
            function(){
                console.log('Error: Post Failed');
            }
        )
    };
    factory.tester = function(){
        console.log('connected to deletingFactory');
    };
    return factory;
}]);

//API Post Service
app.factory('postingFactory', ['$http', '$state', function($http, $state){
    var factory = {};
    factory.poster = function(mail, handler, redirectTo){
        $http({
            method: 'POST',
            url: apiUrl + handler,
            data: $.param(mail),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(){
                console.log('Post Successful');
                $state.go(redirectTo);
            },
            function(){
                console.log('Error: Post Failed');
            }
        )
    };
    factory.tester = function(){
        console.log('connected to postingFactory');
    };
    return factory;
}]);

function MyCMSConfig($stateProvider, $urlRouterProvider) {
	"use strict";

	$stateProvider
        .state("site",{
            url:"/",
            templateUrl:"views/site/site.html",
        })
        .state("login", {
            url: "/login",
            templateUrl: "views/login.html",
            controller: ["$log", "$state", "gettingFactory", LoginStateCtrl],
            controllerAs: "login"
        })
        .state("main", {
            url: "/main",
            templateUrl: "views/main.html"
        })
        .state("main.pages", {
            url: "/pages",
            templateUrl: "views/pages.html",
            controller:[ "$state", "gettingFactory", "deletingFactory", PageCtrl ],
            controllerAs: "page"
        })
        .state("main.dashboard", {
            url: "/dashboard",
            templateUrl: "views/dashboard.html",
            controller: ["gettingFactory", DashboardCtrl],
            controllerAs: "dashboard"
        })
        .state("main.users", {
            url: "/users",
            templateUrl: "views/users.html",
            controller: [ "$state", "gettingFactory", "deletingFactory", userCtrl ],
            controllerAs: "user"
        })
        .state("main.settings", {
            url: "/settings",
            templateUrl: "views/settings.html"
        })
        .state("main.pageEditor", {
            url: "/pageEditor/:pageId",
            templateUrl: "views/pageEditor.html",
            controller: [ "$state", "$stateParams", "gettingFactory", "postingFactory", pageEditorCtrl ],
            controllerAs: "pageEdit"
        })
        .state("main.userEditor", {
           url: "/userEditor/:userId",
           templateUrl: "views/userEditor.html",
           controller: [ "$state", "$stateParams", "gettingFactory", userEditorCtrl ],
           controllerAs: "userEdit"
        });

	$urlRouterProvider.otherwise("/login");
}

function MyCMSCtrl() {
	"use strict";
}

function LoginStateCtrl($log, $state, gettingFactory) {
	"use strict";

	var vm = this;
    vm.loginCreds = false;
	vm.status = "";

    gettingFactory.getter('users')
        .then(function(response){
            vm.users = response.data;
        }, function(errResonse){
            console.error('Error occured fetching data.')
    });

	vm.loginCheck = function() {
		for (var i = 0; i < vm.users.length; i++ ){
            if(vm.userDetails.username == vm.users[i].username && vm.userDetails.password == vm.users[i].password){
                vm.loginCreds = true;
            }
        }

        if(vm.loginCreds){
            $state.transitionTo("main.dashboard");
        } else {
            console.log("Invalid Credentials");
        }

        /*
        if (vm.userDetails.username !== vm.userDetails.password) {
			vm.status = "Incorrect login";
			return;
		}*/

	}
}

function DashboardCtrl(gettingFactory){
    "use strict";
    var vm = this;
    gettingFactory.getter('updates')
        .then(function(response){
            vm.updates = response.data;
        }, function(errResonse){
            console.error('Error occured fetching data.')
        });
}

function MainStateCtrl($state) {
	"use strict";
}

function PageCtrl($state, gettingFactory, deletingFactory){
	"use strict";

	var vm = this;
    vm.showWarning = false;

	//vm.pages = pages;

    gettingFactory.getter('pages')
    .then(function(response){
        vm.pages = response.data;
    }, function(errResonse){
        console.error('Error occured fetching data.')
    });

    vm.editPage = function(index){
		console.log(">>edit pages " + index);
		$state.go("main.pageEditor", {pageId: index});
    };

    vm.preDelete = function(index){
        vm.data = {pageToDelete: vm.pages[index].name};
        vm.showWarning = true;
    };

    vm.deletePage = function(){
        if(vm.delString === "DELETE"){
            vm.delString = "";
            vm.showWarning = false;
            deletingFactory.deleter(vm.data, 'pages');
        }
    };

    vm.abort = function(){
        vm.showWarning = false;
        vm.data = {};
        vm.delString = "";
    };

	vm.removePage = function(index){
        this.data = {pageToDelete: vm.pages[index].name};
        deletingFactory.deleter(this.data, 'pages');
    };

	vm.addPage = function(){
        $state.go("main.pageEditor", {pageId: vm.pages.length});
	};

}

function pageEditorCtrl($state, $stateParams, gettingFactory, postingFactory){
    "use strict";

    var vm = this;
    gettingFactory.getter('pages')
    .then(function(response){
        vm.pages = response.data;
        console.log($stateParams);
        if ($stateParams.pageId < vm.pages.length) {
            vm.page = vm.pages[$stateParams.pageId];
            vm.name = vm.page.name;
            vm.author = vm.page.author;
            vm.lastmodified = vm.page["last-modified"];
        }
    }, function(errResonse){
        console.error('Error occured fetching data.')
    });

    vm.submitChange = function(){
        vm.newData = {
            newPage : {
                name: vm.name,
                author: vm.author,
                "last-modified": (new Date()).toString().slice(0,21),
                "header-img": vm.img,
                caption: vm.caption,
                idNum: $stateParams.pageId
            }
        };
        console.log(vm.newData);
        postingFactory.poster(vm.newData, 'pages', "main.pages");

        /*vm.page.name = vm.name;
        vm.page.author = vm.author;
        vm.page["last-modified"] = (new Date()).toString().slice(0,21);
        vm.page.HeaderImg = vm.img;
        vm.page.caption = vm.caption; */

        //add event to updates
        //var update = vm.lastmodified + ":" + login.userDetails.username + " has update the page " + vm.name;
        //updates.push(update);
        //console.log(update);
    }
}

function userCtrl($state, gettingFactory, deletingFactory){
    "use strict";

    var vm = this;

    //vm.users = users;
    gettingFactory.getter('users')
    .then(function(response){
        vm.users = response.data;
    }, function(errResonse){
        console.error('Error occured fetching data.')
    });

    vm.edit = function(index){
        $state.go("main.userEditor", {userId: index});
    };

    vm.addUser = function(){
      $state.go("main.userEditor", {userId: vm.users.length});
    };

    vm.deleteUser = function(index){
        this.data = {usernameToDelete: vm.users[index].username};
        deletingFactory.deleter(this.data, 'users');
    };
}

function userEditorCtrl($state, $stateParams){
    "use strict";

    var vm = this;
    vm.permissions = [
    'Administrator', 'Editor', 'SomethingElse'
    ];
    if ($stateParams.userId < users.length){
        vm.user = users[$stateParams.userId];
        vm.username = vm.user.username;
        vm.name = vm.user.name;
        vm.email = vm.user.email;
        vm.permission = vm.user.permission;
    }

    vm.submitChange = function(){
        users[$stateParams.userId] =
            {username: vm.username, name: vm.name, email: vm.email, permission: vm.perm, password: vm.password};
        $state.go("main.users");
    };
}
