var Page = require('../models/pages.js');
var User = require('../models/users.js');
var Update = require('../models/updates.js');

Page.find(function(err, pages){
    if(err) return console.error(err);
    if(pages.length) return;
    new Page({
        name:"Singapore",
        author: "Fred",
        "last-modified": "not modified",
        "header-img": "assets/places/singapore.jpg",
        caption: "What I love most about Singapore are the sharp and constant contrasts: smoky temples in the shadow of skyscrapers, luxe condos backing onto jungle, old-school shophouses housing secret cocktail dens. I can travel the world without ever leaving the island.",
        idNum: 0
    }).save();
    new Page({
        name:"Malaysia",
        author: "Ted",
        "last-modified": "not modified",
        "header-img": "assets/places/malaysia.jpg",
        caption: "Malaysia is like two countries in one, cleaved in half by the South China Sea. While peninsula flaunts bustling cities, colonial architecture, misty tea plantations and chill-out islands, Malaysian Borneo hosts wild jungles of orangutans, granite peaks and remote tribes, along with some pretty spectacular diving.",
        idNum: 1
    }).save();
    new Page({
        name:"Thailand",
        author: "Wilma",
        "last-modified": "not modified",
        "header-img": "assets/places/thailand.jpg",
        caption:"It's easy to say that the thing I love most about Thailand is Thai food. But then I'm reminded of that feeling of freedom during a motorcycle trip upcountry. And of the sensory overload of a busy morning market – or a night out in Bangkok. And of encounters with history and culture, the new and the old, at just about every turn.",
        idNum: 2
    }).save();
});

User.find(function(err, users){
    if(err) return console.error(err);
    if(users.length) return;
    new User({
        username:"Tedx",
        name:"Ted",
        email: "admin@stackup.sg",
        permission: "Administrator",
        password: "changeme"
    }).save();
    new User({
        username:"admin",
        name:"Wilma",
        email: "Wilma@stackup.sg",
        permission: "Administrator",
        password: "admin"
    }).save();
    new User({
        username:"Charles",
        name:"Charles",
        email: "charles@stackup.sg",
        permission: "Administrator",
        password: "changeme"
    }).save();
    new User({
        username:"Frank",
        name:"Frank",
        email: "Frank@stackup.sg",
        permission: "Content",
        password: "changeme"
    }).save();
});

Update.find(function(err, updates){
    if(err) return console.log(err);
    if(updates.length) return;
    new Update({
        author: "Fred",
        description: "Admin has updated the blog page",
        updatedAt: "updated_at"
    }).save();
    new Update({
        author: "Wilma",
        description: "User has added a new blog page",
        updatedAt: "updated_at"
    }).save();
    new Update({
        author: "Ted",
        description: "New user has been added as administrator",
        updatedAt: "updated_at"
    }).save();
})

