var mongoose = require('mongoose');

var pagesSchema = mongoose.Schema({
	name: String,
    author: String,
    "last-modified": String,
    "header-img": String,
    caption: String,
    idNum: Number
});

var Page = mongoose.model('Page', pagesSchema);
module.exports = Page;