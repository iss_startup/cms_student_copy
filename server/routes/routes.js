var express = require('express');
var router = express.Router();

var Page = require('../models/pages.js');
var User = require('../models/users.js');
var Update = require('../models/updates.js');

//middlewear for all requests
router.use(function(req, res, next){
    //Use a middlewear to do validation to make sure requests are safe
    //For logging and everything else
    console.log("received a request: " + req.method);
    // console.log(req);
    next(); //Callback to the next route
});

//route: api/
router.get('/', function(req, res){
    res.send('Stackup CMS API');
});

router.route('/users')
    .get(function(req, res){
        User.find(function(err, users){
	        if(err) return res.status(500).send('Error: ' + err);
	        res.json(users.map(function(user){
	            return{
	                username:user.username,
	                name: user.name,
	                email: user.email,
	                permission: user.permission,
	                password: user.password
	            }
	        }));
    	});
    })
    .post(function(req, res){
    	User.
    })

router.route('/users/:userID')
    .get(function(req,res){
        console.log(req.params);
        User.find({username: req.params.userID}, function(err, user){
            if(err) return res.status(500).send('Error: ' + err);
            res.json(user);
        })
    })

router.route('/pages')
    .get(function(req,res){
        Page.find(function(err, pages){
            if(err) return res.status(500).send('Error ' + err);
            res.json(pages.map(function(page){
                return{
                    name: page.name,
                    author: page.author,
                    "last-modified": page['last-modified'],
                    "header-img": page['header-img'],
                    caption: page.caption,
                    idNum: page.idNum
                }
            }));
        });
    })

router.route('/updates')
    .get(function(req, res){
        Update.find(function(err, pages){
            if(err) return res.status(500).send('Error: ' + err)
            res.json(pages.map(function(update){
                return{
                    author: update.author,
                    description: update.description,
                    updatedAt: update.updatedAt
                }
            }))
        })
    })

module.exports = router