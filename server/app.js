var express = require('express');
var handlebars = require('express-handlebars');
var cors = require('cors');
var app = express();


app.engine('handlebars', handlebars({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

//Connecting to local DB
var mongoose = require('mongoose');
var opts = {
    server : {
        socketOption: {keepAlive: 1}
    }
};

switch(app.get('env')){
    case 'development':
        mongoose.connect('mongodb://localhost/cms_db', opts);
        break;
    case 'production':
        mongoose.connect('mongodb://localhost/cms_db', opts);
        break;
    default:
        throw new Error('Unknown execution environment:' + app.get('env'));
}

//Import routes
var routes = require('./routes/routes.js');

//Schemas for Mongo
var Page = require('./models/pages.js');
var User = require('./models/users.js');
var Update = require('./models/updates.js');

//Mongo DB commands
//Seed initial data
require('./models/data.js');

//Body parser for parsing request
app.use(require('body-parser').urlencoded({extended: true}));


app.use('/api', cors());

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();



//routes will be prefixed with api/
app.use('/api', routes);

app.post('/api/users/del', cors(), function(req, res){
    console.log('Delete req for username: ' + req.body.usernameToDelete);
    User.remove({username: req.body.usernameToDelete},
        function(err){
            if(err){
                console.log(err.stack);
                return res.sendStatus(201);
            }
            console.log('Successfully removed above!!!');
            return res.sendStatus(201);
        }
    );
});


app.post('api/updates', function(req, res){
    Update.update(
        {$set: 
            {
                author: req.body.author,
                description: req.body.description,
                updatedAt: req.body.updatedAt
            }
     },
  
    {upsert: true},
    function(err){
        if(err){
            console.log(err.stack);
            return res.sendStatus(500);
        }
       console.log('Successfully userted above!!!');
        return res.sendStatus(202);
    }
    )
});

app.post('/api/pages/del', cors(), function(req, res){
    console.log('Delete req for page: ' + req.body.pageToDelete);
    Page.remove({name: req.body.pageToDelete},
        function(err){
            if(err){
                console.log(err.stack);
                return res.sendStatus(500);
            }
            console.log('Successfully removed above!!!');
            return res.sendStatus(202);
        }
    );
});

app.post('/api/pages', function(req, res){
    console.log(req.body.newPage);
    Page.update(
        {idNum: req.body.newPage.idNum},
        {$set : {
            name: req.body.newPage.name,
            author: req.body.newPage.author,
            "last-modified": req.body.newPage["last-modified"],
            "header-img": req.body.newPage["header-img"],
            caption: req.body.newPage.caption,
        }},
        {upsert: true},
        function(err){
            if(err){
                console.error(err.stack);
                return res.sendStatus(500);
            }
            console.log('Successfully userted above!!!');
            return res.sendStatus(202);
        }
    );
});

app.use(express.static(__dirname + "/public"));

app.set('port', process.env.APP_PORT || 8000);

app.listen(app.get('port'), function(){
    console.log('Express started on http://localhost:' + app.get('port') + '; press Ctrl-C to terminate.');
});